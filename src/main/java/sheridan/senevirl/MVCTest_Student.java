/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.senevirl;

public class MVCTest_Student {
    public static void main(String[] args){
        
        Student model = retrieveStudentFromDatabase();
        
        StudentView view = new StudentView();
        StudentController controller = new StudentController(model, view);
        
        controller.updateView();
        
        controller.setStudentName("Alex");
        controller.updateView();
         
    }
      private static Student retrieveStudentFromDatabase(){
          Student student = new Student();
          student.setName("Levan");
          student.setId("974839473");
            return student;
        }
}
