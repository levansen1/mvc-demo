/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.senevirl;

public class StudentController {
  private Student model;
  private StudentView view;
    
  
 StudentController(Student model, StudentView view){
this.model = model;
this.view = view;
}

 public void setStudentName(String name){
     model.setName(name);
 }


 public String getStudentName(){
     return model.getName();
 }


 public void setStudentId(String id){
     model.setId(id);
 }

 public String getStudentId(){
     return model.getId();
 }

 public void updateView(){
     view.printStudentDetails(model.getName(), model.getId());
 }
 
}

